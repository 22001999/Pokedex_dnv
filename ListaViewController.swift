//
//  ListaViewController.swift
//  Pokedéx_dnv
//
//  Created by COTEMIG on 04/08/22.
//

import UIKit

class ListaViewController: UIViewController, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return listaNames.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "minhaCell", for: indexPath) as! Celula
            cell.label.text = listaNames[indexPath.row]
        
        return cell
    }
    

    @IBOutlet weak var tableView: UITableView!
    
    var listaNames: [String] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.dataSource = self
        
        listaNames.append("Bulbassauro")
        listaNames.append("Ivyssauro")
        listaNames.append("Venussauro")
        listaNames.append("Charmander")
        listaNames.append("Charmeleon")
        listaNames.append("Charizard")
        listaNames.append("Squirtle")
        listaNames.append("Wartortle")
        listaNames.append("Blastoise")
        listaNames.append("Pikachu")

        // Do any additional setup after loading the view.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
