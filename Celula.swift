//
//  Celula.swift
//  Pokedéx_dnv
//
//  Created by COTEMIG on 23/06/22.
//

import UIKit

class Celula: UITableViewCell {

    @IBOutlet weak var label: UILabel!
    @IBOutlet weak var view: UIView!
    @IBOutlet weak var img: UIImageView!
    @IBOutlet weak var tituloView: UILabel!
    @IBOutlet weak var Specification: UITextField!
    @IBOutlet weak var Poision: UITextField!
    @IBOutlet weak var Number: UILabel!
}
